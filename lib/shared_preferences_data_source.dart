import 'dart:convert';

import 'package:core_model/network/response/signin.dart';
import 'package:core_model/preferences/keep_signed_in.dart';
import 'package:core_model/preferences/localization_setting.dart';
import 'package:core_shared_preferences/shared_preferences_data_source_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesDataSource implements SharedPreferencesDataSourceService{
  static const String sessionKey = "SESSION";
  static const String keepSignedInKey = "KEEP_SIGNED_IN";
  static const String localizationSettingKey = "LOCALIZATION_SETTING";
  final Future<SharedPreferences> _preferences = SharedPreferences.getInstance();

  @override
  Future<bool> removeSession() async {
    SharedPreferences preferences = await _preferences;
    return await preferences.remove(sessionKey);
  }

  @override
  Future<void> saveSession(SignIn session) async {
    SharedPreferences preferences = await _preferences;
    preferences.setString(sessionKey, jsonEncode(session));
  }

  @override
  Future<void> removeAll() async {
    SharedPreferences preferences = await _preferences;
    for (var key in preferences.getKeys()) {
      preferences.remove(key);
    }
  }

  @override
  Future<SignIn?> getSession() async {
    SharedPreferences preferences = await _preferences;
    String? session = preferences.getString(sessionKey);
    if(session!=null){
      SignIn signIn = SignIn.fromJson(jsonDecode(session));
      return signIn;
    }else{
      return null;
    }
  }

  @override
  Future<void> keepSignedIn(KeepSignedIn keepSignIn) async {
    SharedPreferences preferences = await _preferences;
    preferences.setString(keepSignedInKey, jsonEncode(keepSignIn));
  }

  @override
  Future<KeepSignedIn?> getKeepSignedIn() async {
    SharedPreferences preferences = await _preferences;
    String? session = preferences.getString(keepSignedInKey);
    if(session!=null){
      KeepSignedIn keepSignIn = KeepSignedIn.fromJson(jsonDecode(session));
      return keepSignIn;
    }else{
      return null;
    }
  }

  @override
  Future<void> removekeepSignedIn() async {
    SharedPreferences preferences = await _preferences;
    preferences.remove(keepSignedInKey);
  }

  @override
  Future<LocalizationSetting> getLocalizationSetting() async {
    SharedPreferences preferences = await _preferences;
    String? result = preferences.getString(localizationSettingKey);
    if(result != null){
      LocalizationSetting localizationSetting = LocalizationSetting.fromJson(jsonDecode(result));
      return localizationSetting;
    }else{
      return LocalizationSetting.createDefault();
    }
  }

  @override
  Future<bool> setLocalizationSetting(LocalizationSetting localizationSetting) async {
    SharedPreferences preferences = await _preferences;
    return preferences.setString(localizationSettingKey, jsonEncode(localizationSetting.toJson()));
  }
  
  @override
  Future<bool> removeLocalizationSetting() async {
    SharedPreferences preferences = await _preferences;
    return preferences.remove(localizationSettingKey);
  }

}