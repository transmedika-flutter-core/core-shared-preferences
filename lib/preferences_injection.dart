import 'package:core_shared_preferences/shared_preferences_data_source.dart';
import 'package:core_shared_preferences/shared_preferences_data_source_service.dart';
import 'package:get_it/get_it.dart';

class PreferenceInjection {
  static final GetIt _getIt = GetIt.instance;

  static void registerDependencies() {
    if (!_getIt.isRegistered<SharedPreferencesDataSourceService>()) {
      _getIt.registerSingleton<SharedPreferencesDataSourceService>(
          SharedPreferencesDataSource());
    }
  }

  static void unRegisterDependencies() {
    if (_getIt.isRegistered<SharedPreferencesDataSourceService>()) {
      _getIt.unregister<SharedPreferencesDataSourceService>();
    }
  }

  static T get<T extends Object>() {
    return _getIt.get<T>();
  }
}
