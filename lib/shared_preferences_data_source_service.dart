import 'package:core_model/network/response/signin.dart';
import 'package:core_model/preferences/keep_signed_in.dart';
import 'package:core_model/preferences/localization_setting.dart';

abstract class SharedPreferencesDataSourceService{
  Future<void> removeAll();
  Future<void> saveSession(SignIn session) ;
  Future<bool> removeSession();
  Future<SignIn?> getSession();
  Future<void> keepSignedIn(KeepSignedIn keepSignedIn);
  Future<void> removekeepSignedIn();
  Future<KeepSignedIn?> getKeepSignedIn();
  Future<bool> setLocalizationSetting(LocalizationSetting localizationSetting);
  Future<LocalizationSetting?> getLocalizationSetting();
  Future<bool> removeLocalizationSetting();
}